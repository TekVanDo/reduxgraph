/**
 * Created by Tek on 19.11.2015.
 */
import _ from 'lodash';
import vis from 'vis';
import { graphConfig } from 'constants/graphConfig'
import { bindActionCreators } from 'redux';
import { GraphHelperService } from 'services/GraphHelperService';
import differ from 'deep-diff';
import * as graphActionCreators from 'actions/graph';
import * as unitsActionCreators from 'actions/units';
import * as actionLinksCreators from 'actions/links';

export class GraphService {
  constructor(initStage) {
    //actions
    this.graphActions = bindActionCreators(graphActionCreators, initStage.dispatch);
    this.unitsActions = bindActionCreators(unitsActionCreators, initStage.dispatch);
    this.actionsLinks = bindActionCreators(actionLinksCreators, initStage.dispatch);

    this.lastStage = initStage;
    this.filteredNodes = GraphHelperService.filterUnits(initStage.units.units, initStage.tags.selectedTags);
    this.graphInstance = null;

    this.graphData = {
      nodes: new vis.DataSet(this.filteredNodes),
      edges: new vis.DataSet(this.lastStage.links.links)
    };

    this.updateActions = {
      'links.links': (newStage, newLinks, restOfPath) => {
        if (newLinks.kind === 'A') {
          let item = newLinks.item;
          if (item.kind === 'N') {
            return this.graphData.edges.add(item.rhs);
          }

          if (item.kind === 'D') {
            this.graphData.edges.clear();
            return this.graphData.edges.add(newStage.links.links);
          }
        }
        if (newLinks.kind === 'E' && restOfPath[1] === 'label') {
          let link = newStage.links.links[restOfPath[0]];
          this.graphData.edges.update([{id: link.id, label: newLinks.rhs}]);
        }
      },
      'links.tempDerivedLinks': (newStage, newLinks, restOfPath) => {
        if (newLinks.item.kind === "D") {
          this.graphData.edges.remove({id: newLinks.item.lhs.id});
        }
      },
      'selectedMode': (NewStage, newSelectedMode) => {
        this.setGraphMode(newSelectedMode.rhs);
      },
      'tags.selectedTags': (NewStage, diff) => {
        this.filteredNodes = GraphHelperService.filterUnits(NewStage.units.units, NewStage.tags.selectedTags);
        let data = {
          nodes: new vis.DataSet(this.filteredNodes),
          edges: new vis.DataSet(NewStage.links.links)
        };
        this.initGraph(initStage.identifier, data);
      },
      'units.units': (NewStage, newUnits, restOfPath) => {
        if(newUnits.kind === 'E'){
          let unit = NewStage.units.units[restOfPath[0]];
          this.graphData.nodes.update([unit]);
        }
        if (newUnits.item && newUnits.item.kind === 'N') {
          this.graphData.nodes.add(newUnits.item.rhs);
        }
        if (newUnits.item && newUnits.item.kind === 'D') {
          this.graphData.nodes.clear();
          return this.graphData.nodes.add(GraphHelperService.filterUnits(NewStage.units.units, NewStage.tags.selectedTags));
        }
      },
      'zoom': (NewStage, newZoom) => {
        this.graphInstance.moveTo({scale: newZoom.rhs});
      }
    };

    this.update.bind(this);
    this.initGraph(initStage.identifier, this.graphData);
  }

  setGraphMode(mode) {
    if (mode === 'drug') {
      this.actionsLinks.clearTempDerivedLinks();
      return this.graphInstance.disableEditMode();
    }
    if (mode === 'addTransformedLink') {
      this.actionsLinks.clearTempDerivedLinks();
      this.graphInstance.addEdgeMode();
    }
    if (mode === 'addDerivedLinks') {
      this.graphInstance.addEdgeMode();
    }
    if (mode === 'addUnit') {
      this.graphInstance.addNodeMode();
    }
  };

  onEdgeAdded(newEdgeData, callback) {
    let obj = {
      from: newEdgeData.from,
      to: newEdgeData.to
    };
    if (newEdgeData.from === newEdgeData.to || _.find(this.lastStage.links.links, obj)
      || _.find(this.lastStage.links.tempDerivedLinks, obj)) {
      return;
    }

    var createLinkFromEdge = (newEdgeData, type) => {
      let links = this.lastStage.links.tempDerivedLinks.length > 0 ? this.lastStage.links.tempDerivedLinks : this.lastStage.links.links;
      let max = _.max(links, 'id');
      max = (max.id) ? max.id : 0;
      return Object.assign({}, newEdgeData, {
        type: type,
        label: '',
        id: max + 1//todo вынести
      });
    };

    if (this.lastStage.selectedMode === 'addTransformedLink') {
      this.actionsLinks.setEditableLinks([createLinkFromEdge(newEdgeData, 'transformed')]);
    }
    if (this.lastStage.selectedMode === 'addDerivedLinks') {
      if (this.lastStage.links.tempDerivedLinks.length > 0 && newEdgeData.to !== this.lastStage.links.tempDerivedLinks[0].to) {
        return;
      }
      let item = createLinkFromEdge(newEdgeData, 'derived');
      callback(item);
      this.actionsLinks.pushTempDerivedLinks([item]);
      this.graphInstance.addEdgeMode();
    }
  }

  onNodeAdded(data) {
    let createNewNode = () => {
      return {
        tags: [],
        id: "",
        Abbreviation: "",
        Description: "",
        isBase: false,
        group: 0
      };
    };
    this.unitsActions.setEditedNodes([createNewNode()]);
  }

  initGraph(graphId, data) {
    let container = document.getElementById(graphId);

    graphConfig.manipulation = {
      addEdge: (data, callback) => {
        this.onEdgeAdded(data, callback);
      },
      addNode: (data, callback) => {
        this.onNodeAdded(data);
      }
    };

    this.graphInstance = new vis.Network(container, data, graphConfig);

    this.graphInstance.on("zoom", (params) => {
      this.graphActions.setZoom(params.scale);
    });

    this.graphInstance.on("click", (params) => {
      if (params.nodes.length > 0 || this.lastStage.units.selectedUnits.length > 0) {
        this.unitsActions.setSelectedNodes(params.nodes);
      }
    });
    this.setGraphMode(data.selectedMode);
    return this;
  }

  update(newState) {
    //math deff between stages
    let diffs = differ(this.lastStage, newState);
    if (diffs) {
      diffs.forEach((diff)=> {
        if (diff.path && diff.path.length > 0) {
          let restOfPath = [...diff.path];
          let path = (restOfPath.length > 2) ? restOfPath.splice(0, 2) : restOfPath;
          path = path.join('.');
          if (this.updateActions[path]) {
            this.updateActions[path](newState, diff, restOfPath);
          }
        }
      });
    }

    this.lastStage = newState;
  }
}