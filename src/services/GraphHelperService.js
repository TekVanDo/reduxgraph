/**
 * Created by Tek on 19.11.2015.
 */
import _ from 'lodash';
export class GraphHelperService {
  static filterUnits(units, selectedTags) {
    if (selectedTags.length === 0) {
      return units;
    }
    return _.filter(units, (one) => {
      return _.some(one.tags, (tag) => {
        return _.some(selectedTags, (selectedTag) => {
          return tag.id === selectedTag.id;
        });
      });
    });
  }

  static findNodeWithLinks(nodeName, nodes, links) {
    let node = _.find(nodes, {id: nodeName});
    let nodeLinks = _.reduce(links, (obj, link)=> {
      if(link.type === 'derived'){
        if (link.from === nodeName) {
          obj.derived.from.push(link);
        }
        if (link.to === nodeName) {
          obj.derived.to.push(link);
        }
      }else if(link.from === nodeName || link.to === nodeName){
        obj.transformed.push(link);
      }
      return obj;
    }, {derived: {from: [], to: []}, transformed: []});
    return {node, nodeLinks};
  }
}