/**
 * Created by Tek on 23.11.2015.
 */
import React, { Component } from 'react';
import {RadioButtonGroup, ToggledButton} from 'components/ToggledButtons';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

/* actions */
import * as actionCreators from 'actions/graph';

@connect((state) => { return {  selectedMode: state.graph.mode } })
export class GraphModeButtons extends Component {
  static propTypes = {
    dispatch: React.PropTypes.func
  };

  constructor(props) {
    super(props);
    this.actions = bindActionCreators(actionCreators, this.props.dispatch);
    this.selectMode = this.selectMode.bind(this);
  }

  selectMode(mode) {
    this.actions.selectMode(mode.mode);
  }

  render() {
    var selectedMode = this.props.selectedMode;
    const modes = [
      {label:'drug', mode: 'drug'},
      {label:'add derived link', mode:'addDerivedLinks'},
      {label:'add transformed link', mode:'addTransformedLink'},
      {label:'add unit', mode:'addUnit'}
    ];

    let equalFunc = (selected, obj) => {
      return selected === obj.mode;
    };

    return (
      <RadioButtonGroup equalFn={equalFunc} selectItem={this.selectMode} selected={selectedMode}>
        {modes.map((x, i) =>
          <ToggledButton buttonObj={x} className="btn btn-primary" key={i + 1}>{x.label}</ToggledButton>
        )}
      </RadioButtonGroup>
    );
  }
}