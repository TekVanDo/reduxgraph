/**
 * Created by Tek on 26.11.2015.
 */
import ListItem from 'material-ui/lib/lists/list-item';
import React, { Component } from 'react';
import ListDivider from 'material-ui/lib/lists/list-divider';
import FontAwesome from 'react-fontawesome';

export class DerivedLinks extends Component {
  render() {
    let onDelete = (e, links) => {
      e.stopPropagation();
      this.props.onDelete(links)
    };

    let onEdit = (links) => {
      this.props.onEdit(links)
    };

    let derived = <div>
      <ListItem primaryText={<i>Derived to</i>} rightIconButton={<FontAwesome onClick={(e) => onDelete(e, this.props.to)}
                                  name="remove" style={{margin: "11px 10px 0 0"}}/>}/>
      {this.props.to.map((el, i) => <DerivedLink onEdit={() => onEdit(this.props.to)}
                                                  key={i}
                                                  linkObj={el}/>
      )}
      <ListItem primaryText={<i>Derived from</i>} rightIconButton={<FontAwesome onClick={(e) => onDelete(e,this.props.from)}
                                  name="remove" style={{margin: "11px 10px 0 0"}}/>}/>
      {this.props.from.map((el, i) =>  <DerivedLink onEdit={() => onEdit(this.props.from)}
                                                    key={i}
                                                    linkObj={el}/>
      )}
      <ListDivider />
    </div>;

    return (
      <div>
        {(this.props.to.length > 0 || this.props.from.length > 0) ? derived : null}
      </div>
    );
  }
}

class DerivedLink extends Component {
  render() {
    let icon;
    let onDelete;
    let text = `from ${this.props.linkObj.from} to ${this.props.linkObj.to}`;

    if (this.props.onDelete) {
      onDelete = (e) => {
        e.stopPropagation();
        this.props.onDelete([this.props.linkObj])
      };
      icon = <FontAwesome onClick={onDelete}
                          name="remove" style={{margin: "11px 10px 0 0"}}/>;
    }

    return (
      <ListItem onClick={() => this.props.onEdit([this.props.linkObj])}
                secondaryText={text}
                rightIconButton={icon}/>
    )
  }
}


export class TransformedLinks extends Component {
  render() {
    let list = <div>
      <ListItem primaryText={<i>Transformed</i>}/>
      {this.props.data.map((el, i) =>  <DerivedLink onDelete={this.props.onDelete}
                                                    onEdit={this.props.onEdit}
                                                    key={i}
                                                    linkObj={el}/>
      )}
    </div>;

    return (
      <div>
        {(this.props.data.length > 0) ? list : null}
      </div>
    )
  }
}
