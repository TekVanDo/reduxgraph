import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { GraphService } from 'services/GraphService';
import vis from 'vis';

/* component styles */
import styles from './styles';

@connect((state) => {
  return {
    tags: state.tags,
    units: state.units,
    links: state.links,
    selectedMode: state.graph.mode,
    zoom: state.graph.zoom
  }
})
export class Graph extends Component {
  constructor(props) {
    super(props);
    this.networkService = null;
    this.state = {};
  }

  static defaultProps = {
    identifier: 'test1'
  };

  componentDidMount() {
    this.initGraph();
  }

  componentWillReceiveProps(nextStage) {
    this.updateGraph(nextStage);
  }

  updateGraph(nextStage) {
    this.networkService.update(nextStage);
  }

  initGraph() {
    this.networkService = new GraphService(this.props);
  };

  render() {
    return (
      <div>
        <div className={styles} id={this.props.identifier}></div>
      </div>
    );
  }
}