import React, { Component } from 'react';
import Dialog from 'material-ui/lib/dialog';
import TextField from 'material-ui/lib/text-field';
import FlatButton from 'material-ui/lib/flat-button';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Tabs from 'material-ui/lib/tabs/tabs';
import Tab from 'material-ui/lib/tabs/tab';
injectTapEventPlugin();
export class DerivedLinkDialog extends Component {
  render() {
    let opend = this.props.links.length > 0 && this.props.links[0].type === 'derived';
    let data = [...this.props.links];

    let onSend = () => {
      this.props.saveAction([...data]);
    };

    let onChangeLabels = (event,i) => {
      data[i] = Object.assign({}, data[i], {label: event.target.value});
    };

    let customActions = [
      <FlatButton
        label="Cancel"
        secondary={true}
        onTouchTap={this.props.cancelAction}
        onClick={this.props.cancelAction}/>,
      <FlatButton
        label="Submit"
        primary={true}
        onClick={onSend}
        onTouchTap={onSend}/>
    ];


    return (
      <div>
        {data && opend ? <Dialog title="Edit Link" open={opend} actions={customActions}>
          <div className="row">
            <div className="col-md-12">
              <TextField ref="from" floatingLabelText="to" defaultValue={data[0].to} disabled={true} fullWidth={true}/>
            </div>
            <div className="col-md-12">
              <Tabs>
                {data.map((one, i)=> <Tab label={i+1} value={''+i} key={i}>
                  <TextField ref="to" floatingLabelText="from" defaultValue={one.from} disabled={true} fullWidth={true}/>
                  <TextField ref="type" floatingLabelText="type" defaultValue={one.type} disabled={true}
                             fullWidth={true}/>
                  <TextField onChange={(e) => {onChangeLabels(e,i)}} ref="label" floatingLabelText="label" defaultValue={one.label} fullWidth={true}/>
                </Tab>)}
              </Tabs>
            </div>
          </div>
        </Dialog>: null}
      </div>
    )
  }
}