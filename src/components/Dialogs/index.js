import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { LinkDialog } from 'components/Dialogs/LinkDialog'
import { UnitDialog } from 'components/Dialogs/UnitDialog'
import { DerivedLinkDialog } from 'components/Dialogs/DerivedLinkDialog'

import * as actionLinksCreators from 'actions/links';
import * as actionModeCreators from 'actions/graph';
import * as unitsActionCreators from 'actions/units';

/* component styles */
@connect((state) => {
  return {
    tags: state.tags,
    groups: state.groups.groups,
    editableLinks: state.links.editableLinks,
    editedUnits: state.units.editedUnits
  }
})
export class Dialogs extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.actionsLinks = bindActionCreators(actionLinksCreators, this.props.dispatch);
    this.actionsMode = bindActionCreators(actionModeCreators, this.props.dispatch);
    this.actionsUnits = bindActionCreators(unitsActionCreators, this.props.dispatch);

    this.saveLinks = (links) => {
      this.actionsLinks.updateLinks(links);
      this.actionsMode.selectMode('drug');
    };
    this.cancelLinkEdit = () => {
      this.actionsLinks.setEditableLinks([]);

      this.actionsMode.selectMode('drug');
    };

    this.saveUnitsEdit = (units) => {
      this.actionsUnits.updateUnits(units);
      this.actionsMode.selectMode('drug');
    };
    this.cancelUnitsEdit = () => {
      this.actionsUnits.setEditedNodes([]);
      this.actionsMode.selectMode('drug');
    };
  }

  render() {
    return (
      <div>
        <LinkDialog links={this.props.editableLinks} saveAction={this.saveLinks} cancelAction={this.cancelLinkEdit}/>
        <DerivedLinkDialog links={this.props.editableLinks} saveAction={this.saveLinks}
                           cancelAction={this.cancelLinkEdit}/>

        <UnitDialog saveAction={this.saveUnitsEdit} cancelAction={this.cancelUnitsEdit} tags={this.props.tags}
                    groups={this.props.groups} units={this.props.editedUnits}/>
      </div>
    );
  }
}