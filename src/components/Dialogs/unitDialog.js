import _ from 'lodash'
import React, { Component } from 'react';
import Dialog from 'material-ui/lib/dialog';
import TextField from 'material-ui/lib/text-field';
import FlatButton from 'material-ui/lib/flat-button';
import Toggle from 'material-ui/lib/toggle'
import SelectField from 'material-ui/lib/select-field'
import {ToggledButton, ToggledButtonGroup} from 'components/ToggledButtons'

export class UnitDialog extends Component {
  render() {
    let opend = this.props.units.length > 0;
    let data = (this.props.units.length > 0) ? this.props.units[0] : null;
    let changeVal = 0;
    let onSend = () => {
      let unit = Object.assign({}, data, {
        Abbreviation: this.refs.Abbreviation.getValue(),
        Description: this.refs.Description.getValue(),
        label: this.refs.Label.getValue(),
        group: changeVal,
        isBase: this.refs.isBase.isToggled(),
        id: this.refs.id.getValue()
      });

      this.props.saveAction([unit]);
    };

    let onSelectChange = (event, val) => {
      changeVal = val;
    };

    let customActions = [
      <FlatButton
        key="CancelUnit"
        label="Cancel"
        secondary={true}
        onTouchTap={this.props.cancelAction}
        onClick={this.props.cancelAction}/>,
      <FlatButton
        key="SubmitUnit"
        label="Submit"
        primary={true}
        onClick={onSend}
        onTouchTap={onSend}/>
    ];

    let selectItem = (tag) => {
      data.tags.push(tag);
      this.setState(data.tags);
    };
    let deselectItem = (tag) => {
      data.tags = _.remove(data.tags, tag);
    };

    //<ToggledButtonGroup selectItem={selectItem} deselectItem={deselectItem}
    //                    selected={data.tags}>
    //  {this.props.tags.tags.map((x, i) =>
    //    <ToggledButton buttonObj={x} className="btn btn-info" key={i + 1}>{x.tag}</ToggledButton>
    //  )}
    //</ToggledButtonGroup>


    return (
      <Dialog title="Edit Link" open={opend} actions={customActions}>
        {data ? <div className="row">
          <SelectField onChange={onSelectChange} ref="group" fullWidth={true} defaultValue={data.group} menuItems={this.props.groups}/>
          <Toggle ref="isBase" name="isBase" defaultValue={data.inBase} label="isBase" fullWidth={true}/>
          <TextField ref="id" className="col-md-12" disabled={data.id !== ''} defaultValue={data.id} floatingLabelText="id"  fullWidth={true}/>
          <TextField ref="Abbreviation" className="col-md-12" defaultValue={data.Abbreviation} floatingLabelText="Abbreviation" fullWidth={true}/>
          <TextField ref="Description" className="col-md-12" defaultValue={data.Description} floatingLabelText="Description" fullWidth={true}/>
          <TextField ref="Label" className="col-md-12" defaultValue={data.label} floatingLabelText="Label" fullWidth={true}/>

        </div>: null}
      </Dialog>
    )
  }
}