import React, { Component } from 'react';
import Dialog from 'material-ui/lib/dialog';
import TextField from 'material-ui/lib/text-field';
import FlatButton from 'material-ui/lib/flat-button';

export class LinkDialog extends Component {
  render() {
    let opend = this.props.links.length > 0 && this.props.links[0].type === 'transformed';
    let data = (this.props.links.length > 0) ? this.props.links[0] : null;

    let onSend = () => {
      let link = Object.assign({}, data, {
        label: this.refs.label.getValue()
      });

      this.props.saveAction([link]);
    };

    let customActions = [
      <FlatButton
        key="cancelLink"
        label="Cancel"
        secondary={true}
        onTouchTap={this.props.cancelAction}
        onClick={this.props.cancelAction}/>,
      <FlatButton
        key="submitLink"
        label="Submit"
        primary={true}
        onClick={onSend}
        onTouchTap={onSend}/>
    ];

    return (
      <Dialog title="Edit Link" open={opend} actions={customActions}>
        {data ? <div className="row">
          <div className="col-md-12">
            <TextField ref="from" floatingLabelText="from" defaultValue={data.from} disabled={true} fullWidth={true}/>
          </div>
          <div className="col-md-12">
            <TextField ref="to" floatingLabelText="to" defaultValue={data.to} disabled={true} fullWidth={true}/>
          </div>
          <div className="col-md-12">
            <TextField ref="type" floatingLabelText="type" defaultValue={data.type} disabled={true}
                       fullWidth={true}/>
          </div>
          <div className="col-md-12">
            <TextField ref="label" floatingLabelText="label" defaultValue={data.label} fullWidth={true}/>
          </div>
        </div> : null}
      </Dialog>
    )
  }
}