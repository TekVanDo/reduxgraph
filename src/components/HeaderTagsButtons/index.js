import React, { Component } from 'react';
import {ToggledButtonGroup, ToggledButton} from 'components/ToggledButtons';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

/* actions */
import * as actionCreators from 'actions/tags';

@connect((state) => { return {  tags: state.tags} })
export class HeaderTagsButtons extends Component {
  static propTypes = {
    dispatch: React.PropTypes.func,
    tags: React.PropTypes.object
  };

  constructor(props) {
    super(props);
    this.actions = bindActionCreators(actionCreators, this.props.dispatch);
    this.selectItem = this.selectItem.bind(this);
    this.deselectItem = this.deselectItem.bind(this);
  }

  selectItem(tag) {
    this.actions.selectTag(tag);
  }

  deselectItem(id) {
    this.actions.deselectTag(id);
  }

  render() {
    var selectedTags = this.props.tags.selectedTags;
    var tags = this.props.tags.tags;
    return (
      <ToggledButtonGroup selectItem={this.selectItem} deselectItem={this.deselectItem} selected={selectedTags}>
        {tags.map((x, i) =>
          <ToggledButton buttonObj={x} className="btn btn-info" key={i + 1}>{x.tag}</ToggledButton>
        )}
      </ToggledButtonGroup>
    );
  }
}