import React, { Component } from 'react';
import FlatButton from 'material-ui/lib/flat-button';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

/* actions */
import * as actionCreators from 'actions/links';

@connect((state) => { return {  tempDerivedLinks: state.links.tempDerivedLinks} })
export class SaveDerivedLinks extends Component {
  static propTypes = {
    dispatch: React.PropTypes.func,
    tempDerivedLinks: React.PropTypes.array
  };

  constructor(props) {
    super(props);
    this.actions = bindActionCreators(actionCreators, this.props.dispatch);
    this.saveDerivedLinks = this.saveDerivedLinks.bind(this);
  }

  saveDerivedLinks(){
    this.actions.setEditableLinks(this.props.tempDerivedLinks);
    this.actions.clearTempDerivedLinks();
  }


  render() {
    return (
      <div>
        {(this.props.tempDerivedLinks.length > 1)?<FlatButton onClick={this.saveDerivedLinks}  label="save derived links" />:null}
      </div>
    );
  }
}