import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { GraphHelperService } from 'services/GraphHelperService';
import RaisedButton from 'material-ui/lib/raised-button';
import LeftNav from 'material-ui/lib/left-nav';
import List from 'material-ui/lib/lists/list';
import ListDivider from 'material-ui/lib/lists/list-divider';
import ListItem from 'material-ui/lib/lists/list-item';
import {DerivedLinks, TransformedLinks} from 'components/NodeLinks'
import Colors from 'material-ui/lib/styles/colors';

/* component styles */
//import styles from './styles';
import * as actionLinksCreators from 'actions/links';
import * as unitsActionCreators from 'actions/units';

@connect((state) => {
  return {
    selectedUnits: state.units.selectedUnits,
    units: state.units.units,
    links: state.links.links
  }
})
export class NodeSideMenu extends Component {
  constructor(props) {
    super(props);
    this.dockedOpen = false;
    this.selectedNode = null;
    this.actionsLinks = bindActionCreators(actionLinksCreators, this.props.dispatch);
    this.unitsActions = bindActionCreators(unitsActionCreators, this.props.dispatch);

    this.editTransformedLink = (links) => {
      this.actionsLinks.setEditableLinks(links)
    };
    this.deleteLinks = (links) =>{
      links.forEach((link)=>{
        this.actionsLinks.deleteLinks([link]);
      });
    };

    this.deleteNode = () => {
      this.unitsActions.deleteUnits([this.selectedNode.node])
    };

    this.editNode = () => {
      this.unitsActions.setEditedNodes([this.selectedNode.node])
    };
  }

  componentDidMount() {
    this.activeCheck(this.props);
  }

  componentWillReceiveProps(nextStage) {
    this.activeCheck(nextStage);
  }

  activeCheck(nextStage) {
    this.dockedOpen = nextStage.selectedUnits.length > 0;
    if (this.dockedOpen) {
      this.selectedNode = GraphHelperService.findNodeWithLinks(nextStage.selectedUnits[0], nextStage.units, nextStage.links);
    } else {
      this.selectedNode = null;
    }
  }

  render() {
    let list = null;
    if (this.selectedNode) {
      list = <List>
        <ListItem primaryText={this.selectedNode.node.Abbreviation}/>
        <ListDivider />
        <ListItem onClick={this.deleteNode} style={{backgroundColor: Colors.red500}} primaryText="Delete"/>
        <ListItem onClick={this.editNode} style={{backgroundColor: Colors.yellow500}} primaryText="Edit"/>
        <ListDivider />
        <ListItem primaryText={<b>Links</b>}/>
        <DerivedLinks onDelete={this.deleteLinks} onEdit={this.editTransformedLink} from={this.selectedNode.nodeLinks.derived.from}
                      to={this.selectedNode.nodeLinks.derived.to}/>
        <TransformedLinks onDelete={this.deleteLinks} onEdit={this.editTransformedLink}
                          data={this.selectedNode.nodeLinks.transformed}/>
        <ListDivider />
      </List>
    }
    return (
      <LeftNav docked={this.dockedOpen} openRight={true}>
        {this.selectedNode ? list : null}
      </LeftNav>
    );
  }
}
