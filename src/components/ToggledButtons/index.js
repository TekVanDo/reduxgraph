/**
 * Created by Tek on 13.11.2015.
 */
import React, { Component } from 'react';
import RaisedButton from 'material-ui/lib/raised-button';

export class ToggledButton extends Component {
  static propTypes = {
    //buttonObj: React.PropTypes.node.isRequired
  };

  render() {
    const {clickAction, children, isSelected, buttonObj} = this.props;
    let classes = this.props.className;
    if (isSelected) {
      classes += ' active';
    }
    return (
      <RaisedButton secondary={true} className="m-t-xxs" onClick={() => clickAction(isSelected, buttonObj)}
                    className={classes} label={children} labelStyle={{borderRadius: '0'}}/>
    );
  }
}

export class ToggledButtonGroup extends Component {
  static propTypes = {
    selected: React.PropTypes.array.isRequired,
    selectItem: React.PropTypes.func.isRequired,
    deselectItem: React.PropTypes.func.isRequired
  };

  renderChildren() {
    const { deselectItem, selectItem, selected, children} = this.props;
    return React.Children.map(children, (child) => {
      return React.cloneElement(child, {
        isSelected: selected.some((el) => el.id === child.props.buttonObj.id),
        clickAction: (isSelected, data) => (isSelected) ? deselectItem(data.id) : selectItem(data)
      });
    });
  }

  render() {
    return (
      <div className="btn-group">
        {this.renderChildren()}
      </div>
    )
  }
}

export class RadioButtonGroup extends Component {
  static propTypes = {
    selected: React.PropTypes.node.isRequired,
    selectItem: React.PropTypes.func.isRequired
  };

  renderChildren() {
    const { deselectItem, selectItem, selected, children} = this.props;

    let equalFn = (selected, obj) => {
      if(this.props.equalFn){
        return this.props.equalFn(selected, obj);
      }
      return selected === obj.buttonObj;
    };
    return React.Children.map(children, (child) => {
      return React.cloneElement(child, {
        isSelected: equalFn(selected, child.props.buttonObj),
        clickAction: (isSelected, data) => selectItem(data)
      });
    });
  }

  render() {
    return (
      <div className="btn-group">
        {this.renderChildren()}
      </div>
    )
  }
}