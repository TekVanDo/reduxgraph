import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

/* actions */
import * as actionCreators from 'actions/graph';
/* component styles */
import styles from './styles';

@connect((state) => { return {  zoom: state.graph.zoom } })
export class ZoomGraph extends Component {
  static propTypes = {
    dispatch: React.PropTypes.func
  };

  constructor(props) {
    super(props);
    this.actions = bindActionCreators(actionCreators, this.props.dispatch);
    this.increment = this.increment.bind(this);
    this.decrement = this.decrement.bind(this);
    this.scale = 0.3;
  }

  increment(){
    this.actions.setZoom(this.props.zoom + this.scale);
  }

  decrement(){
    this.actions.setZoom(this.props.zoom - this.scale);
  }

  render() {
    return (
      <div className={`${styles}`}>
        <div onClick={this.increment} className="graphZoomIncrementButton"><span>+</span></div>
        <div onClick={this.decrement} className="graphZoomDecrementButton"><span>&ndash;</span></div>
      </div>
    );
  }
}