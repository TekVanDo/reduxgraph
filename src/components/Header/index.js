import React, { Component } from 'react';
import { Link } from 'react-router';
import Toolbar from 'material-ui/lib/toolbar/toolbar';
import ToolbarGroup from 'material-ui/lib/toolbar/toolbar-group';
import ToolbarSeparator from 'material-ui/lib/toolbar/toolbar-separator';
import {HeaderTagsButtons} from 'components/HeaderTagsButtons';
import {GraphModeButtons} from 'components/GraphModeButtons';
import {SaveDerivedLinks} from 'components/SaveDerivedLinks';
import FlatButton from 'material-ui/lib/flat-button';

/* component styles */
import styles from './styles';

export class Header extends Component {
  render() {
    return (
    <Toolbar className={`${styles}`}>
      <ToolbarGroup key={0} float="left">
        <HeaderTagsButtons></HeaderTagsButtons>
      </ToolbarGroup>
      <ToolbarGroup key={1} float="right">
        <div className="btn-group">
          <SaveDerivedLinks></SaveDerivedLinks>
        </div>
        <ToolbarSeparator />
      </ToolbarGroup>
      <ToolbarGroup key={2} float="right">
        <GraphModeButtons></GraphModeButtons>
      </ToolbarGroup>
    </Toolbar>
    );
  }
}
