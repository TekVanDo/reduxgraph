/**
 * Created by Tek on 23.11.2015.
 */
const initialState = {
  mode: 'drug',
  zoom: 1
};

export function graph(state = initialState, action) {
  switch (action.type) {
    case 'SELECT_MODE':
      return Object.assign({}, state, {
        mode: action.mode
      });

    case 'SET_ZOOM':
      return Object.assign({}, state, {
        zoom: action.zoom
      });

    default:
      return state;
  }
}
