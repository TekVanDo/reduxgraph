import _ from 'lodash';
const initialState = {
  units: [
    {id: "100_sm", Abbreviation: "100 SM", Description: "", isBase: false, group: 0, label: 'ss2', tags:[
      {id: 1, tag: "metric", description: "Metric unit system"},
      {id: 2, tag: "imperial", description: "Imperial unit system"}
    ]},
    {id: "1000_sm", Abbreviation: "1000 SM", Description: "", isBase: true, group: 0, label: 'ss', tags:[
      {id: 1, tag: "metric", description: "Metric unit system"}
    ]},

    {id: "kg/m", Abbreviation: "kg/m", Description: "", isBase: false, group: 1, label: 'kg/m', tags:[
      {id: 1, tag: "metric", description: "Metric unit system"},
      {id: 2, tag: "imperial", description: "Imperial unit system"}
    ]},
    {id: "m", Abbreviation: "m", Description: "", isBase: false, group: 1,label: 'm', tags:[]},
    {id: "kg", Abbreviation: "kg", Description: "", isBase: false, group: 1, label: 'kg', tags:[
      {id: 1, tag: "metric", description: "Metric unit system"},
      {id: 2, tag: "imperial", description: "Imperial unit system"}
    ]},

    {id: "sec", Abbreviation: "1982-1984=1", Description: "", isBase: false, label: 'test', group: 1, tags:[]},
    {id: "min", Abbreviation: "1982-1984=1", Description: "", isBase: false, label: 'test1', group: 1, tags:[]}
  ],
  selectedUnits: [],
  editedUnits: []
};

export function units(state = initialState, action) {
  switch (action.type) {
    case 'SET_SELECTED_NODES':
      return  {
        ...state,
        selectedUnits: [...action.selectedUnits]
      };

    case 'SET_EDITED_NODES':
      return  {
        ...state,
        editedUnits: [...action.editedUnits]
      };

    case 'UPDATE_NODES':
      let newUnits = action.units;
      let units = _.map(state.units, (unit) => {
        let changedUnit = _.find(newUnits, {id: unit.id});
        if (changedUnit) {
          newUnits = _.remove(newUnits, (one) => !(changedUnit.id === one.id));
          return changedUnit;
        }
        return unit;
      });
      units = units.concat(newUnits);
      return {
        ...state,
        units: [...units],
        editedUnits: []
      };

    case 'DELETE_NODES':
      let filteredNodes = _.without(state.units, ...action.units);
      return {
        ...state,
        units: [...filteredNodes],
        selectedUnits: [],
        editedUnits: []
      };

    default:
      return state;
  }
}
