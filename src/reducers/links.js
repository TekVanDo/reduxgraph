import _ from 'lodash';
const initialState = {
  links: [
    {id: 1, from: "100_sm", to: "1000_sm", type: "transformed", label: '1'},

    {id: 2, from: "kg", to: "kg/m", type: "derived", label: '2'},
    {id: 3, from: "m", to: "kg/m", type: "derived", label: '3'},

    {id: 4, from: "sec", to: "min", type: "transformed", label: '4'}
  ],
  editableLinks: [],
  tempDerivedLinks: []
};

export function links(state = initialState, action) {
  switch (action.type) {
    case 'ADD_LINKS':
      return {
        ...state,
        selectedTags: [
          ...state.links, ...action.links
        ]
      };

    case 'UPDATE_LINKS':
      let newLinks = action.links;
      let links = _.map(state.links, (link) => {
        let changedLink = _.find(newLinks, {id: link.id});
        if (changedLink) {
          newLinks = _.remove(newLinks, (one) => !(changedLink.id === one.id));
          return changedLink;
        }
        return link;
      });
      links = links.concat(newLinks);
      return {
        ...state,
        links: [...links],
        editableLinks: []
      };

    case 'SET_EDITABLE_LINKS':
      return {
        ...state,
        editableLinks: [...action.links]
      };

    case 'DELETE_LINKS':
      let filteredLinks = _.without(state.links, ...action.links);
      return {
        ...state,
        links: [...filteredLinks]
      };

    case 'PUSH_TEMP_DERIVED_LINKS':
      return {...state,
        tempDerivedLinks: [...state.tempDerivedLinks, ...action.tempDerivedLinks]
      };

    case 'CLEAR_TEMP_DERIVED_LINKS':
      return {
        ...state,
        tempDerivedLinks: []
      };

    default:
      return state;
  }
}
