import { combineReducers } from 'redux';
import { routeReducer } from 'redux-simple-router';
import {reducer as formReducer} from 'redux-form';
import { nodeSideMenu } from './nodeSideMenu';
import { tags } from './tags';
import { links } from './links';
import { units } from './units';
import { graph } from './graph';
import { groups } from './groups';

const rootReducer = combineReducers({
  form: formReducer,
  routing: routeReducer,
  tags,
  links,
  units,
  graph,
  groups,
  nodeSideMenu
});

export default rootReducer;