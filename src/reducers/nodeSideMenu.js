const initialState = {
    sideMenu:false
};
export function nodeSideMenu(state = initialState, action) {
    switch (action.type) {
        case 'SHOW_SIDE_MENU':
            return {
                ...state,
                items: true
            };

        case 'HIDE_SIDE_MENU':
            return {
                ...state,
                items: false
            };

        default:
            return state;
    }
}