import _ from 'lodash';
const initialState = {
  tags: [
    {id: 1, tag: "metric", description: "Metric unit system"},
    {id: 2, tag: "imperial", description: "Imperial unit system"},
    {id: 3, tag: "usd", description: "United States dollars"}
  ],
  selectedTags: []
};

export function tags(state = initialState, action) {
  switch (action.type) {
    case 'SELECT_TAG':
      return {
        ...state,
        selectedTags: [
          ...state.selectedTags, action.tag
        ]
      };

    case 'DESELECT_TAG':
      return {
        ...state,
        selectedTags: state.selectedTags.filter((one)=> one.id !== action.id)
      };

    default:
      return state;
  }
}
