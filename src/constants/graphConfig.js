export const graphConfig = {
  autoResize: true,
  layout: {randomSeed: 2},
  nodes: {
    shape: 'circle',
    size: 30,
    font: {
      size: 32,
      color: '#ffffff'
    },
    borderWidth: 2,
    borderWidthSelected: 5
  },
  edges: {
    width: 2,
    font: {
      size: 16,
      color: '#ffffff'
    },
    selectionWidth: 5
  }
};