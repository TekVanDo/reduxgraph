/**
 * Created by Tek on 23.11.2015.
 */
export function selectMode(mode) {
  return {
    type: 'SELECT_MODE',
    mode
  };
}

export function setZoom(zoom) {
  return {
    type: 'SET_ZOOM',
    zoom
  };
}