export function selectTag(tag) {
  return {
    type: 'SELECT_TAG',
    tag
  };
}

export function deselectTag(id) {
  return {
    type: 'DESELECT_TAG',
    id
  };
}
