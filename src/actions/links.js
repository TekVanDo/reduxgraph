/**
 * Created by Tek on 26.11.2015.
 */
export function addLinks (links) {
  return {
    type: 'ADD_LINKS',
    links
  };
}

export function deleteLinks(links) {
  return {
    type: 'DELETE_LINKS',
    links
  };
}

export function setEditableLinks(links) {
  return {
    type: 'SET_EDITABLE_LINKS',
    links
  };
}

export function updateLinks(links) {
  return {
    type: 'UPDATE_LINKS',
    links
  };
}

export function editLink(link) {
  return {
    type: 'EDITE_LINK',
    link
  };
}

export function pushTempDerivedLinks(tempDerivedLinks) {
  return {
    type: 'PUSH_TEMP_DERIVED_LINKS',
    tempDerivedLinks
  };
}

export function clearTempDerivedLinks() {
  return {
    type: 'CLEAR_TEMP_DERIVED_LINKS'
  };
}