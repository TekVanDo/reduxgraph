/**
 * Created by Tek on 23.11.2015.
 */
export function setSelectedNodes(selectedUnits) {
  return {
    type: 'SET_SELECTED_NODES',
    selectedUnits
  };
}

export function setEditedNodes(editedUnits) {
  return {
    type: 'SET_EDITED_NODES',
    editedUnits
  };
}

export function updateUnits(units) {
  return {
    type: 'UPDATE_NODES',
    units
  };
}

export function deleteUnits(units) {
  return {
    type: 'DELETE_NODES',
    units
  };
}