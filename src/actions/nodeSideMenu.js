export function showSideMenu(index) {
    return {
        type: 'SHOW_SIDE_MENU',
        index
    };
}

export function hideSideMenu() {
    return {
        type: 'HIDE_SIDE_MENU'
    };
}
