import React, { Component } from 'react';
import DocumentMeta from 'react-document-meta';
/* components */
import { Graph } from 'components/Graph'
import { ZoomGraph } from 'components/ZoomGraph'
import { Dialogs } from 'components/Dialogs'

import styles from './styles';

const metaData = {
  title: 'Redux Easy Boilerplate',
  description: 'Start you project easy and fast with modern tools',
  canonical: 'http://example.com/path/to/page',
  meta: {
    charset: 'utf-8',
    name: {
      keywords: 'react,meta,document,html,tags'
    }
  }
};
export class Home extends Component {
  render() {
    return (
      <section className={`${styles}`}>
        <DocumentMeta { ...metaData } />
        <div className="graphMain">
          <ZoomGraph />
          <Graph />
          <Dialogs />
        </div>
      </section>
    );
  }
}
